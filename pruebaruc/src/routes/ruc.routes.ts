// path: api/products
import { Router } from 'express';
import { create, getByRuc, getRucByUrl} from '../controllers/ruc.controller';

const router = Router();

router.post('/register', create);
router.get('/search/:ruc', getByRuc);
router.get('/api', getRucByUrl);

export default router;