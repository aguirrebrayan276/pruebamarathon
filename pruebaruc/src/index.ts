import express from 'express';
import { config } from 'dotenv';
import dbConnection from './database/config';
import RucRoutes from './routes/ruc.routes';
 

config();
dbConnection();

const app = express();
app.use(express.json());

app.get('/', (_,response) => {
	response.send({message: "Server Running Successfully"})
});

app.use('/ruc', RucRoutes); 

app.listen(process.env.PORT || 5000, () => {
	console.log(`Server is running on PORT: ${process.env.PORT || 5000}`);
});
