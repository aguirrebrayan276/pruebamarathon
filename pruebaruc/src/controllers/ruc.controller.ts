import { Request, Response } from 'express';
import { networkSuccess, serverError, networkError } from '../middlewares/response.middleware';
import Ruc from '../models/Ruc.model';

const axios = require('axios');

const create = async (req: Request, res: Response) => {
	try { 
		const ruc = new Ruc(req.body); 
        //@ts-ignore
		await ruc.save();
		networkSuccess({res,message: 'RUC creado.', status: 201, data: ruc}) 
		return;
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema', error});
	}
};

const getRucByUrl = async (req: Request, res: Response) => {
	try {
		//const { param1, param2 } = req.query;
		const { ruc } = req.query;
		const { tipo } = req.query;
        const apiUrl = `http://wsruc.com/Ruc2WS_JSON.php?tipo=${tipo}&ruc=${ruc}&token=cXdlcnR5bGFtYXJja19zYUBob3RtYWlsLmNvbXF3ZXJ0eQ==`;
		const response = await axios.get(apiUrl);
		networkSuccess({res,message: `Consulta de RUC: ${ruc}, de tipo: ${tipo}`, data: response.data})
		return;
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema', error});
	}
} 

const getByRuc = async (req: Request, res: Response) => {
	try {
		const { ruc } = req.params;
		const body = await Ruc.findOne({ruc}); 
		if(body){
			networkSuccess({res,message: `RUC encontrado: ${ruc}`,data: body}) 
		}else {
			networkError({res, message: 'RUC no encontrado.', status: 404});
		}
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema', error});
	}
} 

export {
	create, getByRuc , getRucByUrl
}